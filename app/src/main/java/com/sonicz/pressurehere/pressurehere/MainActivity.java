package com.sonicz.pressurehere.pressurehere;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    TextView text, text2, text3, text4;
    android.hardware.SensorManager sm;
    Sensor pre;
    double pressure = 0;
    double alt = -1000;
    LocationManager lm;
    Location loc;
    MyLocationListener mll = new MyLocationListener();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.textView);
        text2 = (TextView) findViewById(R.id.textView2);
        text3 = (TextView) findViewById(R.id.textView3);
        text4 = (TextView) findViewById(R.id.textView4);
        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        pre = sm.getDefaultSensor(Sensor.TYPE_PRESSURE);
        sm.registerListener(this, pre, SensorManager.SENSOR_DELAY_UI);
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Turn on GPS?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
        Criteria criteria = new Criteria();
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        String bestProvider = lm.getBestProvider(criteria, true);
        lm.requestLocationUpdates(bestProvider, 1000, 0, mll);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        pressure = event.values[0];
        text.setText("Current pressure:\n" + String.format("%.1f", pressure) + " hPa\n");
        text.append(String.format("%.2f", hPa2inHg(pressure)) + " inHg");
        if (alt != -1000 && pressure != 0) {
            updateQNH(pressure, alt);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    protected void onResume() {
        super.onResume();
        sm.registerListener(this, pre, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

    public void updateQNH(double pre, double alt) {

        double QNH = pre + alt / 9.01;
        text2.setText("Established QNH:\n" + String.format("%.1f", QNH) + " hPa\n");
        text2.append(String.format("%.2f", hPa2inHg(QNH)) + " inHg");
    }

    public double hPa2inHg(double hPa) {
        return hPa * 0.02952998751;
    }

    public double meter2feet(double m) {
        return m * 3.2808399;
    }

    public double kmh2kt(double kmh) {
        return kmh * 0.539956803;
    }

    private class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
            String accu = location.getAccuracy() + "";
            String altt = location.getAltitude() + "";
            String speed = location.getSpeed() + "";
            String message = "GPS altitude: " + String.format("%.1f", meter2feet(Double.valueOf(altt))) + " ft\nGPS speed: " + String.format("%.1f", kmh2kt(Double.valueOf(speed))) + " kts";
            text4.setText(message);
            text4.append("\nGPS accuracy: " + String.format("%.1f", Double.valueOf(accu)) + " m");
            alt = Double.valueOf(altt);
            if (alt != -1000 && pressure != 0) {
                updateQNH(pressure, alt);
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        public void onProviderEnabled(String provider) {

        }

        public void onProviderDisabled(String provider) {
        }
    }
}